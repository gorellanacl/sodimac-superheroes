const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');

// Launching express server
const app = express();

app.use(bodyParser.json());

// Imports
require('./routes/superheroeRoute')(app);

app.listen(config.app.port, () => {
  console.log(`App running on port ${config.app.port}`);
});