const app = {
    port: process.env.APP_PORT || 3001,
    enviroment: process.env.ENV || 'DEV'
};

const api = {
    host: process.env.API_HOST || 'https://superheroapi.com/api/',
    accessToken: process.env.API_ACCESS_TOKEN || '10156979562833391'
}

module.exports = {
    app,
    api
};
