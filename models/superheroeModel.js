const axios = require('axios');
const config = require('../config');

class SuperHeroeModel {

    constructor() {
        this.endpoint = `${config.api.host}${config.api.accessToken}`;
    }

    // Get entire superheroe's information 
    getSuperheroData = async(id) => {
        return this._makeRequest(`${this.endpoint}/${id}`);
    }

    // Search superheroes
    searchSuperheroes = async(name) => {
        return this._makeRequest(`${this.endpoint}/search/${name}`);
    }

    // Request
    _makeRequest = async(endpoint) => {
        let msg = 'No message';

        try {

            const options = {
                url: endpoint,
                method: 'get'
            };

            const res = await axios(options);
            if(!res) { throw { message: 'No response from Superhero API' } }
            if(res.response === 'error') { throw { message: res.error } }

            return res.data;

        } catch (e) {
            console.error(`ERROR: searchSuperheroes`, e.message);
            msg = e.message;
        }

        return { response: 'error', error: msg };
    }

}

module.exports = new SuperHeroeModel();