
const model = require('../models/superheroeModel');

module.exports = (app) => {

    // Get superheroe's information by its id
    app.get('/api/superheroe/:id', async(req, res) => {
        res.json(await model.getSuperheroData(req.params.id || 0));
    });

    // Search superheroes
    app.get('/api/superheroe/search/:name', async(req, res) => {
        res.json(await model.searchSuperheroes(req.params.name || ''));
    });
}