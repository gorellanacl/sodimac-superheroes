# Superhero API

Utiliza API pública de [superheroapi.com](https://superheroapi.com/) para obtener información respecto a todos los superhéroes conocidos.

---

### Requerimientos

1. `Node.js v12.13.1` Descargable de [sitio oficial](https://nodejs.org/es/download/)
2. `npm v6.12.1` Que viene integrado en el paquete de instalación de Node.js

### Tecnologías utilizadas

1. `Node.js` (Express Framework y variados módulos, como: dotenv, body-parser, nodemon, concurrently, etcétera).
2. `React`

### Puesta en marcha
Debería estar todo configurado para iniciar de inmediato la aplicación:

1. Clonar desde el repositorio
``` git
git clone https://gorellanacl@bitbucket.org/gorellanacl/sodimac-superheroes.git
```

2. Estando en la raíz de la aplicación, comenzar la instalación de todas las dependencias
```
> npm install
```

3. Una vez terminado, levantar automáticamente ejecutando el siguiente comando:

```
> npm run dev
```

4. Por defecto, el backend queda expuesto por el puerto 3001 y el frontend, a través del puerto 3000.
Acceder a aplicación a través de http://localhost:3000


## Licencia (ISC)

En caso que nunca hayas escuchado acerca de [ISC license](http://en.wikipedia.org/wiki/ISC_license), este es un equivalente a MIT license.
Ver [LICENSE file](LICENSE) para más detalles.