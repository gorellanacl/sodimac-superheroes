// Hooks!
import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import service from './services/superheroeService';
import Superheroe from './components/Superheroe';

function App() {

  // Initial states
  const [superheroes, setSuperheroes] = useState(null);
  const [criteria, setCriteria] = useState('batman');
  const [criteria2Find, setCriteria2Find] = useState('');
  const [loading, setLoading] = useState(true);
  const [showModal, setShowModal] = useState(false);
  const [sh, setSh] = useState({});

  // Per every renderization
  useEffect(() => {
    if(!superheroes) {
      // It will occurs one time only
      search(criteria);
    }
  });


  // ----------------------------------------------

  // Searcher
  const search = async(name) => {
    setLoading(true);

    const res = await service.searchSuperheroes(name);
    setSuperheroes(res.data.response === 'success' ? res.data.results : []);
    setCriteria(name);
    
    setLoading(false);
  }

  // Return superhero's data
  const findDataById = async(id) => {
    const res = await service.getSuperheroe(id);
    if(res.data.response === 'success') {
      const powerstats = res.data.powerstats;
      const bio = res.data.biography;
      setSh({
        intelligence: powerstats.intelligence,
        strength: powerstats.strength,
        speed: powerstats.speed,
        durability: powerstats.durability,
        power: powerstats.power,
        combat: powerstats.combat,
        fullName: bio['full-name'],
        placeOfBirth: bio['place-of-birth'],
        publisher: bio.publisher,
      });
    } else {
      alert(`Error getting superhero information: ${res.data.error}`);
    }
    handleShowModal();
  }

  // ----------------------------------------------

  // Handling form
  const handleSearchValue = (event) => {
    setCriteria2Find(event.target.value);
  }
  const submitForm = (event) => {
    event.preventDefault();

    const s = criteria2Find.toString().trim();
    if(s !== '') {
      // Go to find with new criteria
      search(s);
    } else {
      alert('No has ingresado ningún criterio para la búsqueda');
    }
  }

  // ----------------------------------------------
  // Handlers
  const handleClose = () => setShowModal(false);
  const handleShowModal = () => setShowModal(true);

  // ----------------------------------------------

  // Rendering
  const renderSuperheroes = (superheroe) => {
    return (
      <tr key={superheroe.id}>
        <td>{superheroe.biography["full-name"]}</td>
        <td><img src={superheroe.image.url} width="50" height="50" /></td>
        <td>{superheroe.appearance.gender}</td>
        <td>{superheroe.appearance.race}</td>
        <td>{superheroe.appearance.height[1]}</td>
        <td>{superheroe.appearance.weight[1]}</td>
        <td><button onClick={(id) => findDataById(superheroe.id)}>Ver</button></td>
      </tr>
    );
  }

  const renderSearcher = () => {
    return (
      <div>
          <span>Buscador:</span>
          <form onSubmit={submitForm}>
            <input
              type="text"
              value={criteria2Find}
              onChange={handleSearchValue} required />
            <button disabled={loading}>Buscar</button>
          </form>
      </div>
    );
  }


  const renderModal = () => {
    return (<Modal show={showModal} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Acerca del Superhéroe...</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Superheroe
          intelligence={sh.intelligence}
          strength={sh.strength}
          speed={sh.speed}
          durability={sh.durability}
          power={sh.power}
          combat={sh.combat}
          fullName={sh.fullName}
          placeOfBirth={sh.placeOfBirth}
          publisher={sh.publisher}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button variant="primary" onClick={handleClose}>Entendido</Button>
      </Modal.Footer>
    </Modal>)
  }

  // ----------------------------------------------

  // Styles
  const tables = {
    borderSpacing: '20px',
    borderCollapse: 'separate'
  };

  // ----------------------------------------------

  return (
    <div className="App">
      { renderModal() }
      { renderSearcher() }
      { !loading ?
      (
        <table style={tables}>
          <tbody>
            <tr>
              <td colSpan="7">Resultados para <b>{criteria}</b></td>
            </tr>
            <tr>
              <th>Nombre</th>
              <th>Imagen</th>
              <th>G&eacute;nero</th>
              <th>Raza</th>
              <th>Altura</th>
              <th>Peso</th>
              <th>Detalles</th>
            </tr>
            {
              (superheroes && superheroes.length > 0) ?
              (
                superheroes.map(superheroe => renderSuperheroes(superheroe))
              ) :
              (<tr colSpan="7"><td>No hay elementos para criterio <b>{criteria}</b></td></tr>)
            }
          </tbody>
        </table>
      ) :
      (
        <p>Cargando Elementos...</p>
      ) 
      }
    </div>
  );
}

export default App;
