const axios = require('axios');

export default {

    getSuperheroe: async (id) => {
        return await axios.get(`/api/superheroe/${id}`);
    },

    searchSuperheroes: async (name) => {
        return await axios.get(`/api/superheroe/search/${name}`);
    }
}