import React from 'react';
import style from '../styles/sh.css';

class Superheroe extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div>
            <div>
                <label className={style.lbl}>Estad&iacute;sticas</label>
                <table>
                    <tbody>
                        <tr><th>Inteligencia</th><td>{this.props.intelligence}</td></tr>
                        <tr><th>Fuerza</th><td>{this.props.strength}</td></tr>
                        <tr><th>Velocidad</th><td>{this.props.speed}</td></tr>
                        <tr><th>Durabilidad</th><td>{this.props.durability}</td></tr>
                        <tr><th>Poder</th><td>{this.props.power}</td></tr>
                        <tr><th>Combate</th><td>{this.props.combat}</td></tr>
                    </tbody>
                </table>
            </div>
            <div style={
                {marginTop:'20px'}
            }>
                <label className={style.lbl}>Biograf&iacute;a</label>
                <table>
                    <tbody>
                        <tr><th>Nombre completo</th><td>{this.props.fullName}</td></tr>
                        <tr><th>Lugar nacimiento</th><td>{this.props.placeOfBirth}</td></tr>
                        <tr><th>Editor</th><td>{this.props.publisher}</td></tr>
                    </tbody>
                </table>
            </div>
        </div>
    );
  }
}

export default Superheroe;